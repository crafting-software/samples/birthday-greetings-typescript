import { expect } from "chai";
import { dedent } from "ts-dedent";

import { Birthdate } from "domain/model/birthdate";
import { EmailAddress } from "domain/model/email_address";
import { FirstName } from "domain/model/first_name";
import { LastName } from "domain/model/last_name";
import { User } from "domain/model/user";
import { Facade } from "infra/api/cli/facade";
import { MemoryUserProvider } from "infra/spi/memory_user_provider";

describe("Facade should", () => {

    const facade = new Facade(new MemoryUserProvider([
        user("Joe Pop", "joe@a.co", "1993-04-10"),
        user("Jim Alton", "jim@a.co", "1998-04-10"),
        user("Jet Selt", "jet@a.co", "2004-02-29"),
        user("Jul Doe", "jul@a.co", "1973-11-18"),
    ]));

    it("list users", () => {
        expect(facade.listUsers()).to.be.equals(
            dedent`
            Joe Pop <joe@a.co>: 1993-04-10
            Jim Alton <jim@a.co>: 1998-04-10
            Jet Selt <jet@a.co>: 2004-02-29
            Jul Doe <jul@a.co>: 1973-11-18
            `);
    });

    it("whish birthday", () => {
        expect(facade.wishBirthday("2022-04-10")).to.be.equals(
            dedent`
            today: 2022-04-10
            - Happy birthday Joe Pop : 1993-04-10
            - Happy birthday Jim Alton : 1998-04-10
            `);
    });
});

function user(fullname: string, email: string, birthday: string): User {
    return User.create(
        FirstName.fromString(fullname.split(" ").shift()),
        LastName.fromString(fullname.split(" ").pop()),
        Birthdate.fromString(birthday),
        EmailAddress.fromString(email));
}
