import { expect } from "chai";
import { DateTime } from "luxon";

import { Birthdate } from "domain/model/birthdate";
import { EmailAddress } from "domain/model/email_address";
import { FirstName } from "domain/model/first_name";
import { LastName } from "domain/model/last_name";
import { User } from "domain/model/user";
import { GreetBirthday } from "domain/usecases/greet_birthday";
import { WishBirthday } from "domain/usecases/wish_birthday";
import { MemoryUserProvider } from "infra/spi/memory_user_provider";

describe("GreetBirthday", () => {

    it("greet nobody birthday scenario", () => {
        expect(birthdayOf({
            today: "2022-01-01", users: [
                user("joe@a.co", "1993-04-10"),
                user("jim@a.co", "1998-04-10"),
                user("jet@a.co", "2004-02-29"),
                user("jul@a.co", "1973-11-18")],
        })).to.be.equals("");
    });

    it("greet birthday simple scenario", () => {
        expect(birthdayOf({
            today: "2022-02-28", users: [
                user("joe@a.co", "1993-04-10"),
                user("jim@a.co", "1998-06-25"),
                user("jet@a.co", "2004-02-29"),
                user("jul@a.co", "1973-11-18")],
        })).to.be.equals("jet@a.co");
    });

    it("greet birthday with two birthdays", () => {
        expect(birthdayOf({
            today: "2022-04-10", users: [
                user("joe@a.co", "1993-04-10"),
                user("jim@a.co", "1998-04-10"),
                user("jet@a.co", "2004-02-29"),
                user("jul@a.co", "1973-11-18")],
        })).to.be.equals("jim@a.co, joe@a.co");
    });

    it("greet birthday for special case on 29 february", () => {
        expect(birthdayOf({
            today: "2022-02-28", users: [
                user("joe@a.co", "1993-04-10"),
                user("jim@a.co", "1998-04-10"),
                user("jet@a.co", "2004-02-29"),
                user("jul@a.co", "1973-11-18")],
        })).to.be.equals("jet@a.co");
    });

    function user(email: string, birthday: string): User {
        return User.create(
            FirstName.fromString(""),
            LastName.fromString(""),
            Birthdate.fromString(birthday),
            EmailAddress.fromString(email));
    }

    interface BirthdayOfParams {
        today: string;
        users: User[];
    }

    function birthdayOf(params: BirthdayOfParams): string {
        const collector = new WishCollector();
        const usecase = new GreetBirthday(new MemoryUserProvider(params.users), collector);
        usecase.processBirthday(DateTime.fromISO(params.today));
        return collector.users.map((u) => u.email.value).sort().join(", ");
    }

    class WishCollector implements WishBirthday {
        public readonly users: User[];

        public constructor() {
            this.users = [];
        }

        public wishFor(user: User): void {
            this.users.push(user);
        }
    }

});
