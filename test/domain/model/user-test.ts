import { expect } from "chai";
import { DateTime } from "luxon";

import { Birthdate } from "domain/model/birthdate";
import { EmailAddress } from "domain/model/email_address";
import { FirstName } from "domain/model/first_name";
import { LastName } from "domain/model/last_name";
import { User } from "domain/model/user";

describe("User", () => {

    describe("hasBirthday should", () => {

        const bob = User.create(
            FirstName.fromString("John"),
            LastName.fromString("Doe"),
            Birthdate.fromString("2000-04-10"),
            EmailAddress.fromString("john.doe@nowhere.com"));

        it("true when birthday", () => {
            expect(bob.hasBirthday(DateTime.fromObject({ year: 2022, month: 4, day: 10 }))).to.be.true;
        });

        it("false otherwise", () => {
            expect(bob.hasBirthday(DateTime.fromObject({ year: 2022, month: 6, day: 4 }))).to.be.false;
        });

    });
});
