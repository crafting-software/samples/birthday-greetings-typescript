
# Birthday message

This project is for training purpose to learn some architectural principles.

1. Hexagonal Architecture, with models and usecases.
2. Immutability whenever possible
3. Use Maybe instead of null
4. Encapsulate all primitives types in value objects


What next ?
- implements différent store (SQL, noSQL, s3, ...)
- implement differents delivery method (rest, command line, html, ...)
- play with different framework (knex, express, ...)

## Layout

    src/                        All sources
      domain/                   Business code, related to the domain
        model/                  The concepts of the domain
        usecases/               The dynamics of the domain
      infra/                    Technical choices
        spi/                    Services needed to operate
        api/                    How we deliver the business
    test/                       All tests files


## Launch unit tests

With a local installation of node, you can

    yarn test
