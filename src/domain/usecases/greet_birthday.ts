import { DateTime } from "luxon";

import { UserProvider } from "./user_provider";
import { WishBirthday } from "./wish_birthday";

export class GreetBirthday {
    public constructor(
        private readonly users: UserProvider,
        private readonly birthday: WishBirthday,
    ) { }

    public processBirthday(today: DateTime) {
        this.users
            .all()
            .filter((user) => user.hasBirthday(today))
            .forEach((user) => this.birthday.wishFor(user));
    }
}
