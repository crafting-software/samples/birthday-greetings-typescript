export class FirstName {
    public static fromString(value: string): FirstName {
        return new FirstName(value);
    }

    private constructor(public readonly value: string) {
    }
}
