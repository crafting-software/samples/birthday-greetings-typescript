import { DateTime } from "luxon";

const FEBRUARY_29 = "2/29";
const FEBRUARY_28 = "2/28";

export class Birthdate {

    public static fromString(value: string): Birthdate {
        return new Birthdate(DateTime.fromISO(value));
    }

    private constructor(public readonly value: DateTime) {
    }

    public isBirthday(today: DateTime): boolean {
        if (today < this.value) {
            return false;
        }
        if (!today.isInLeapYear && this.birthday() === FEBRUARY_29) {
            return monthDay(today) === FEBRUARY_28;
        }
        return this.birthday() === monthDay(today);
    }

    public render(): string {
        return this.value.toISODate();
    }

    private birthday(): string {
        return monthDay(this.value);
    }
}

function monthDay(date: DateTime): string {
    return `${date.month}/${date.day}`;
}
