export class LastName {
    public static fromString(value: string): LastName {
        return new LastName(value);
    }

    private constructor(public readonly value: string) {
    }
}
