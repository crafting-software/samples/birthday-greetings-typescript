import { User } from "domain/model/user";
import { UserProvider } from "domain/usecases/user_provider";

export class MemoryUserProvider implements UserProvider {
    public constructor(
        private readonly users: User[],
    ) { }

    public all(): User[] {
        return this.users;
    }
}
