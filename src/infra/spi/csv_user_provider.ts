import { parse } from "csv-parse/sync";
import * as fs from "fs";

import { Birthdate } from "domain/model/birthdate";
import { EmailAddress } from "domain/model/email_address";
import { FirstName } from "domain/model/first_name";
import { LastName } from "domain/model/last_name";
import { User } from "domain/model/user";
import { UserProvider } from "domain/usecases/user_provider";

function contentOf(filepath: string): string {
    return fs.readFileSync(filepath, { encoding: "utf-8" });
}

export class CSVUserProvider implements UserProvider {
    public constructor(
        private readonly filepath: string,
    ) { }

    public all(): User[] {
        const result = parse(contentOf(this.filepath), {
            columns: ["firstName", "lastName", "birthdate", "email"],
            delimiter: ",",
        });
        return result.map((u) => User.create(
            FirstName.fromString(u.firstName.trim()),
            LastName.fromString(u.lastName.trim()),
            Birthdate.fromString(u.birthdate.trim()),
            EmailAddress.fromString(u.email.trim())));
    }
}
