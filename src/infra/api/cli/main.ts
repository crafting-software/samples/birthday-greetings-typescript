/* tslint:disable:no-console */
const Help = `
Usage:
  cli list users
  cli wish birthday [--date=<date>]
  cli -h | --help
  cli --version

Options:
  --date=<date>  Set date [default: now]
  -h --help      Show this screen.
  --version      Show version.
`;

import { docopt } from "docopt";
import { CSVUserProvider } from "infra/spi/csv_user_provider";
import { Facade } from "./facade";

const options = docopt(Help, { version: "0.1" });

const facade = new Facade(new CSVUserProvider("./data/users.csv"));

if (options.list) {
    if (options.users) {
        console.log(facade.listUsers());
    }
} else if (options.wish) {
    if (options.birthday) {
        console.log(facade.wishBirthday(options["--date"]));
    }
}
